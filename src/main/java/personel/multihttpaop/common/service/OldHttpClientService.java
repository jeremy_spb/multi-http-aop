package personel.multihttpaop.common.service;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import java.io.IOException;

public class OldHttpClientService implements HttpClientService {
    private final HttpClient client;

    public OldHttpClientService(HttpClient client) {
        this.client = client;
    }

    @Override
    public void invoke() throws IOException {
        System.out.println("OLD CLIENT INVOCATION");
        GetMethod method = new GetMethod("http://yandex.ru");
        client.executeMethod(method);
        System.out.println(method.getResponseBodyAsString());
        System.out.println("------------------");
    }
}
