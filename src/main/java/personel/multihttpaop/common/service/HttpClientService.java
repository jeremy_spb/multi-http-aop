package personel.multihttpaop.common.service;

import java.io.IOException;

public interface HttpClientService {
    void invoke() throws IOException;
}
