package personel.multihttpaop.common.service;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;

public class NewHttpClientService implements HttpClientService {
    private final HttpClient client;

    public NewHttpClientService(HttpClient client) {
        this.client = client;
    }

    @Override
    public void invoke() throws IOException {
        System.out.println("NEW CLIENT INVOCATION");
        HttpGet request = new HttpGet("http://yandex.ru");
        final var response = client.execute(request);
        try (final var is = response.getEntity().getContent()) {
            System.out.println(new String(is.readAllBytes()));
        }
        System.out.println("------------------");
    }
}
