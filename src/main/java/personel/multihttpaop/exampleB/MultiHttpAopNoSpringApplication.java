package personel.multihttpaop.exampleB;

import org.apache.commons.httpclient.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import personel.multihttpaop.common.service.NewHttpClientService;
import personel.multihttpaop.common.service.OldHttpClientService;

import java.io.IOException;

public class MultiHttpAopNoSpringApplication {
    public static void main(String[] args) throws IOException {
        final var oldHttpClient = new HttpClient();
        final var newHttpClient = HttpClientBuilder.create()
                .build();
        new OldHttpClientService(oldHttpClient).invoke();
        new NewHttpClientService(newHttpClient).invoke();
    }
}
