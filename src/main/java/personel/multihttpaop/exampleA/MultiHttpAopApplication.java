package personel.multihttpaop.exampleA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import personel.multihttpaop.common.service.NewHttpClientService;
import personel.multihttpaop.common.service.OldHttpClientService;

import java.io.IOException;

@SpringBootApplication
public class MultiHttpAopApplication {

    public static void main(String[] args) throws IOException {
        final ApplicationContext context = SpringApplication.run(MultiHttpAopApplication.class, args);
        invokeOldClient(context);
        invokeNewClient(context);
    }

    private static void invokeOldClient(ApplicationContext context) throws IOException {
        context.getBean(OldHttpClientService.class).invoke();
    }

    private static void invokeNewClient(ApplicationContext context) throws IOException {
        context.getBean(NewHttpClientService.class).invoke();
	}
}
