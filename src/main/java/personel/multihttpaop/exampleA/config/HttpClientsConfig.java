package personel.multihttpaop.exampleA.config;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import personel.multihttpaop.common.service.NewHttpClientService;
import personel.multihttpaop.common.service.OldHttpClientService;

@Configuration
public class HttpClientsConfig {
    @Bean
    public HttpClient newHttpClient() {
        return HttpClientBuilder.create()
                .build();
    }

    @Bean
    public OldHttpClientService oldClientService() {
        return new OldHttpClientService(oldHttpClient());
    }

    @Bean
    public NewHttpClientService newHttpClientService() {
        return new NewHttpClientService(newHttpClient());
    }

    @Bean
    public org.apache.commons.httpclient.HttpClient oldHttpClient() {
        return new org.apache.commons.httpclient.HttpClient();
    }
}
