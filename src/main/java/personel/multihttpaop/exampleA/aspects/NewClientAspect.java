package personel.multihttpaop.exampleA.aspects;

import org.apache.http.HttpRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Component
@Aspect
public class NewClientAspect {
    @Pointcut("target(org.apache.http.client.HttpClient) && execution(public * execute(org.apache.http.client.methods.HttpUriRequest,..))")
    public void aroundExecute() {
    }

    @Around("aroundExecute()")
    public Object log(ProceedingJoinPoint pjp) throws Throwable {
        final var arg = (HttpRequest) pjp.getArgs()[0];
        System.out.println("New aspect for: " + arg.getRequestLine().getUri());
        final var proceed = pjp.proceed();
        System.out.println("After real invocation of new client");
        return proceed;
    }
}
