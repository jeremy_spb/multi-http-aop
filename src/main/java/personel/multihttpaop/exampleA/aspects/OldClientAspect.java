package personel.multihttpaop.exampleA.aspects;

import org.apache.commons.httpclient.HttpMethod;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class OldClientAspect {
    @Pointcut("execution(public * org.apache.commons.httpclient.HttpClient.executeMethod(org.apache.commons.httpclient.HttpMethod))")
    public void aroundExecuteMethod() {
    }

    @Around("aroundExecuteMethod()")
    public Object log(ProceedingJoinPoint pjp) throws Throwable {
        final var arg = (HttpMethod) pjp.getArgs()[0];
        System.out.println("Old aspect for: " + arg.getURI().toString());
        final var proceed = pjp.proceed();
        System.out.println("After real invocation of old client");
        return proceed;
    }
}
